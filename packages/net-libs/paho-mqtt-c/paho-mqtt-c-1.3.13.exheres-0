# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=eclipse project=${PN//-/.} tag=v${PV} ] cmake

SUMMARY="Eclipse Paho C client library for MQTT"

LICENCES="EDL-1.0 EPL-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-apps/util-linux
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DPAHO_BUILD_DEB_PACKAGE:BOOL=FALSE
    -DPAHO_BUILD_DOCUMENTATION:BOOL=FALSE
    -DPAHO_BUILD_SAMPLES:BOOL=FALSE
    -DPAHO_BUILD_SHARED:BOOL=TRUE
    -DPAHO_BUILD_STATIC:BOOL=FALSE
    -DPAHO_ENABLE_CPACK:BOOL=FALSE
    -DPAHO_HIGH_PERFORMANCE:BOOL=FALSE
    -DPAHO_USE_SELECT:BOOL=FALSE
    -DPAHO_WITH_LIBUUID:BOOL=TRUE
    -DPAHO_WITH_SSL:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DPAHO_ENABLE_TESTING:BOOL=TRUE -DPAHO_ENABLE_TESTING:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    edo rm -rf "${IMAGE}"/usr/share/doc/Eclipse\ Paho\ C
}

