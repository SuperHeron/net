# Copyright 2014-2015 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mysql.exlib', which is:
#     Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
#     Copyright 2011-2013 Timo Gurr <tgurr@exherbo.org>
#     Distributed under the terms of the GNU General Public License v2

require cmake pam systemd-service s6-rc-service
require flag-o-matic

export_exlib_phases pkg_pretend src_prepare src_configure src_install

MARIADB_MAJOR_VERSION=$(ever range 1-2)

SUMMARY="An enhanced, drop-in replacement for MySQL"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="mirror://${PN}/${PNV}/source/${PNV}.tar.gz"

REMOTE_IDS="freshcode:${PN}"
UPSTREAM_CHANGELOG="https://${PN}.com/kb/en/${PN}-$(ever delete_all)-changelog [[ lang = [ en ] ]]"
UPSTREAM_DOCUMENTATION="https://${PN}.com/kb/en [[ lang = [ en ] ]]"
UPSTREAM_RELEASE_NOTES="https://${PN}.com/kb/en/${PN}-$(ever delete_all)-release-notes [[ lang = [ en ] ]]"

LICENCES="GPL-2" # with-exceptions
SLOT="0"
MYOPTIONS="
    debug
    embedded-server [[ description = [ A full-featured MariaDB server that can be linked to a client application ] ]]
    jdbc            [[ description = [ Compile CONNECT storage engine with JDBC support ] ]]
    kerberos
    lz4             [[ description = [ Use LZ4 compression for some storage engines ] ]]
    systemd         [[ description = [ Compile with systemd socket activation and notification ] ]]
    tcpd
    zstd            [[ description = [ Use Zstandard compression for some storage engines ] ]]

    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+:
        sys-devel/flex
        virtual/pkg-config
    build+run:
        app-admin/eclectic[>=2.0.18] [[ note = [ Split ld-*.path, @TARGET@ substitution ] ]]
        app-arch/bzip2
        app-arch/snappy
        app-arch/xz
        dev-libs/libedit
        dev-libs/libevent:=[>=1.4.12]
        dev-libs/libxml2:2.0
        dev-libs/pcre2
        group/mysql
        net-misc/curl
        sys-libs/cracklib
        sys-libs/ncurses
        sys-libs/pam
        sys-libs/zlib[>=1.2.3]
        user/mysql
        jdbc? ( virtual/jdk:=[>=1.6] )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        !libc:musl? ( dev-libs/libxcrypt:= )
        lz4? ( app-arch/lz4 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
        zstd? ( app-arch/zstd[>=1.1.3] )
       !dev-db/mysql [[
            description = [ MariaDB is a drop-in replacement for MySQL with same binary names ]
            resolution = uninstall-blocked-after
        ]]
       !dev-db/Percona-Server:* [[
            description = [ Percona-Server is a drop-in replacement for MySQL with same binary names ]
            resolution = uninstall-blocked-after
        ]]
    suggestion:
        app-admin/logrotate [[ description = [ Use logrotate for rotating logs ] ]]
"

if ever at_least 10.6.18 ; then
    DEPENDENCIES+="
        build:
            sys-devel/bison[>=2.4]
        build+run:
            sys-libs/liburing
    "
else
    MYOPTIONS+="
        tokudb [[ description = [ Build the TokuDB high-performance storage engine (deprecated since 10.5) ] ]]
    "
    DEPENDENCIES+="
        build:
            sys-devel/bison[>=2]
        build+run:
            dev-libs/libaio
            tokudb? ( dev-libs/jemalloc )
    "
fi

if ever at_least 10.11.8 ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/fmt[>=10.2.1]
    "
fi

mariadb_pkg_pretend() {
    # Sanity check when upgrading to a different x.y version of mysql
    if [[ -z ${MARIADB_MAJOR_UPGRADE} ]] && has_version ${CATEGORY}/${PN} && \
        ! has_version ${CATEGORY}/${PN}[=${MARIADB_MAJOR_VERSION}*] ; then
        ewarn "To install a different major version of MariaDB, you have to dump/reload your database."
        ewarn "When you've done this, please set 'MARIADB_MAJOR_UPGRADE=YesPlease', to continue the upgrade."
        ewarn "For more information visit:"
        ewarn "https://${PN}.com/kb/en/upgrading-${PN}"
        die "Dump your databases before doing a major version upgrade of MariaDB."
    fi

    if [[ -f "${ROOT}"/etc/tmpfiles.d/${PN}.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/${PN}.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/${PN}.conf and can be safely"
        ewarn "removed after upgrade if you did not make any changes to it."
    fi
}

mariadb_src_prepare() {
    cmake_src_prepare

    if ! ever at_least 10.6.18 ; then
        edo sed -e "s/\"gcc-ar\"/\"($(exhost --tool-prefix)gcc-ar\"/" \
                -e "s/\"gcc-ranlib\"/\"($(exhost --tool-prefix)gcc-ranlib\"/" \
                -i "${CMAKE_SOURCE}"/storage/tokudb/PerconaFT/CMakeLists.txt
    fi

    edo sed -e "/^MY_ADD_TEST(mf_iocache)/d" \
            -i unittest/sql/CMakeLists.txt

    # Disable test which doesn't work out of the box
    edo sed -e "/^MY_ADD_TEST(test-connect)/d" \
            -i unittest/embedded/CMakeLists.txt
}

mariadb_src_configure() {
    # TODO: gcc15 defaults to C23, which breaks due to missing return values
    # and pointer incompatibilities
    append-cflags -std=gnu17
    # Fails to build with gold linker: https://mariadb.atlassian.net/browse/MDEV-5982
    append-ldflags -fuse-ld=bfd

    local cmakeargs=(
        -DBUILD_CONFIG:STRING=mysql_release
        # The default layout STANDALONE breaks all the scripts.
        -DINSTALL_LAYOUT:STRING="RPM"
        -DCOMPILATION_COMMENT:STRING="Exherbo"
        -DDEFAULT_CHARSET:STRING=utf8
        -DDEFAULT_COLLATION:STRING=utf8_general_ci
        -DFEATURE_SET:STRING=community
        -DINSTALL_BINDIR:PATH=bin
        -DINSTALL_DOCDIR:PATH=../share/doc/${PNVR}
        -DINSTALL_DOCREADMEDIR:PATH=../share/doc/${PNVR}
        -DINSTALL_INCLUDEDIR:PATH=include/mysql
        -DINSTALL_INFODIR:PATH=../share/mysql/info
        -DINSTALL_LIBDIR:PATH=lib/mysql
        -DINSTALL_MANDIR:PATH=../share/man
        -DINSTALL_MYSQLSHAREDIR:PATH=../share/mysql
        # Empty value to not install the mysql-test directory
        -DINSTALL_MYSQLTESTDIR:STRING=
        -DINSTALL_PAMDIR:PATH=$(getpam_mod_dir)
        -DINSTALL_PLUGINDIR:PATH=lib/mysql/plugin
        -DINSTALL_SBINDIR:PATH=bin
        -DINSTALL_SCRIPTDIR:PATH=bin
        # aclocal/mysql.m4 goes there
        -DINSTALL_SHAREDIR:PATH=../share
        # Empty value to not install the sql-bench directory
        -DINSTALL_SQLBENCHDIR:STRING=
        -DINSTALL_SUPPORTFILESDIR:PATH=../share/doc/${PNVR}/support-files
        # These paths should be empty so that no files are installed to /etc
        -DINSTALL_SYSCONFDIR:PATH=
        -DINSTALL_SYSCONF2DIR:PATH=
        -DINSTALL_SYSTEMD_SYSUSERSDIR:PATH=/usr/$(exhost --target)/lib/sysusers.d
        -DINSTALL_SYSTEMD_TMPFILESDIR:PATH=${SYSTEMDTMPFILESDIR}
        -DINSTALL_SYSTEMD_UNITDIR:PATH=${SYSTEMDSYSTEMUNITDIR}
        -DMYSQL_DATADIR:PATH=/var/lib/mysql
        -DMYSQL_UNIX_ADDR:PATH=/run/mysqld/mysqld.sock
        -DSYSCONFDIR:PATH=/etc/mysql
        # MongoDB C driver - commented out at the moment, but to be safe
        -DAWS_SDK_EXTERNAL_PROJECT:BOOL=FALSE
        -DCONNECT_WITH_MONGO:BOOL=FALSE
        -DCONNECT_WITH_ODBC:BOOL=FALSE
        -DCONNECT_WITH_ZIP:BOOL=TRUE
        -DENABLE_DTRACE:BOOL=FALSE
        -DENABLE_GCOV:BOOL=FALSE
        -DENABLED_LOCAL_INFILE:BOOL=TRUE
        -DENABLED_PROFILING:BOOL=TRUE
        -DGRN_WITH_KYTEA:STRING=no
        -DGRN_WITH_MESSAGE_PACK:STRING=no
        -DGRN_WITH_MRUBY:BOOL=FALSE
        -DGRN_WITH_ZEROMQ:STRING=no
        -DGRN_WITH_ZLIB:STRING=yes
         # Mroonga requires external tools (http://groonga.org)
        -DPLUGIN_MROONGA:STRING=NO
        # The new client library API_TESTS require a real running server
        -DSKIP_TESTS:BOOL=TRUE
        # Default ENGINES for MariaDB 5.6 community build (currently == xlarge) minus embedded
        -DWITH_ARCHIVE_STORAGE_ENGINE:BOOL=TRUE
        -DWITH_ASAN:BOOL=FALSE
        -DWITH_BLACKHOLE_STORAGE_ENGINE:BOOL=TRUE
        -DWITH_DBUG_TRACE:BOOL=FALSE
        -DWITH_EXTRA_CHARSETS:STRING=all
        -DWITH_FEDERATED_STORAGE_ENGINE:BOOL=TRUE
        -DWITH_GPROF:BOOL=FALSE
        -DWITH_INNOBASE_STORAGE_ENGINE:BOOL=TRUE
        -DWITH_PARTITION_STORAGE_ENGINE:BOOL=TRUE
        -DWITH_PROTECT_STATEMENT_MEMROOT:BOOL=FALSE
        -DWITH_MARIABACKUP:BOOL=TRUE
        -DWITH_MSAN:BOOL=FALSE
        -DWITH_MYSQLD_LDFLAGS="${LDFLAGS}"
        # numa is unwritten
        -DWITH_NUMA:STRING=OFF
        -DWITH_PCRE:STRING=system
        -DWITH_SSL:STRING=system
        -DWITH_TSAN:BOOL=FALSE
        -DWITH_UBSAN:BOOL=FALSE
        -DWITH_UNIT_TESTS:BOOL=$(expecting_tests && echo "TRUE" || echo "FALSE")
        -DWITH_VALGRIND:BOOL=FALSE
        -DWITH_WSREP_ALL:BOOL=FALSE
        -DWITH_ZLIB:STRING=system
        -DWITHOUT_PACKED_SORT_KEYS:BOOL=FALSE
        # boost is needed for the oqgraph storage engine
        -DCMAKE_DISABLE_FIND_PACKAGE_Boost:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
        # See https://mariadb.atlassian.net/browse/MDEV-6449
        -DCMAKE_AR:PATH=$(exhost --tool-prefix)gcc-ar
        -DCMAKE_RANLIB:PATH=$(exhost --tool-prefix)gcc-ranlib
        $(cmake_option jdbc CONNECT_WITH_JDBC)
        -DGRN_WITH_LZ4:STRING=$(option lz4 && echo 'yes' || echo 'no')
        -DGRN_WITH_ZSTD:STRING=$(option zstd && echo 'yes' || echo 'no')
        -DPLUGIN_AUTH_GSSAPI:STRING=$(option kerberos && echo 'DYNAMIC' || echo 'NO')
        -DPLUGIN_AUTH_GSSAPI_CLIENT:STRING=$(option kerberos && echo 'DYNAMIC' || echo 'OFF')
        -DWITH_ROCKSDB_BZip2:STRING=ON
        -DWITH_ROCKSDB_LZ4:STRING=$(option lz4 && echo 'ON' || echo 'OFF')
        -DWITH_ROCKSDB_ZSTD:STRING=$(option zstd && echo 'ON' || echo 'OFF')
        -DWITH_ROCKSDB_snappy:STRING=ON
        $(cmake_with systemd)
        $(cmake_disable_find kerberos GSSAPI)
    )

    if ! ever at_least 10.6.18 ; then
        cmakeargs+=(
            -DWITH_PMEM:BOOL=FALSE
        )
        if option tokudb ; then
            cmakeargs+=(
                # See https://jira.mariadb.org/browse/MDEV-14524
                -DTOKUDB_OK:BOOL=TRUE
                -DPLUGIN_TOKUDB:STRING=YES
                # TokuDB is enabled, but jemalloc is not. This configuration is not supported
                -DWITH_JEMALLOC:STRING=system
                -DWITH_SAFEMALLOC:BOOL=FALSE
            )
        else
            cmakeargs+=(
                -DPLUGIN_TOKUDB:STRING=NO
                -DWITH_JEMALLOC:BOOL=FALSE
            )
        fi
    fi

    if option debug ; then
        cmakeargs+=(
            -DCMAKE_BUILD_TYPE:STRING=Debug
        )
    else
        cmakeargs+=(
            -DCMAKE_BUILD_TYPE:STRING=Release
        )
    fi

    if ever at_least 10.6.18 ; then
        cmakeargs+=(
            -DCLIENT_PLUGIN_ZSTD:STRING=$(option zstd && echo 'DYNAMIC' || echo 'OFF')
            -DCMAKE_DISABLE_FIND_PACKAGE_LIBAIO:BOOL=TRUE
            -DWITH_JEMALLOC:BOOL=FALSE
            -DWITH_LIBAIO:BOOL=FALSE
            -DWITH_URING:BOOL=TRUE
        )
    fi

    if ever at_least 10.11.8 ; then
        cmakeargs+=(
            -DWITH_LIBFMT:STRING=system
            -DPLUGIN_PROVIDER_BZIP2:STRING=DYNAMIC
            -DPLUGIN_PROVIDER_LZ4:STRING=$(option lz4 && echo 'DYNAMIC' || echo 'NO')
            -DPLUGIN_PROVIDER_LZMA:STRING=DYNAMIC
            -DPLUGIN_PROVIDER_LZO:STRING=NO
            -DPLUGIN_PROVIDER_SNAPPY:STRING=DYNAMIC
        )
    else
        cmakeargs+=(
            -DWITH_INNODB_BZIP2:STRING=ON
            -DWITH_INNODB_LZ4:STRING=$(option lz4 && echo 'ON' || echo 'OFF')
            -DWITH_INNODB_LZMA:STRING=ON
            -DWITH_INNODB_LZO:STRING=OFF
            -DWITH_INNODB_SNAPPY:STRING=ON
        )
    fi

    CFLAGS="-fPIC ${CFLAGS} -fno-strict-aliasing -DBIG_JOINS=1 -fomit-frame-pointer -fno-delete-null-pointer-checks -Wno-error"
    CXXFLAGS="-fPIC ${CXXFLAGS} -fno-strict-aliasing -DBIG_JOINS=1 -felide-constructors -fno-delete-null-pointer-checks -Wno-error"

    ecmake \
        "${cmakeargs[@]}" \
        $(cmake_with debug DEBUG) \
        $(cmake_with embedded-server EMBEDDED_SERVER) \
        $(cmake_with tcpd LIBWRAP)
}

mariadb_src_install() {
    cmake_src_install

    keepdir /etc/mysql

    # MYSQL_ADD_EXECUTABLE also installs the executable, which isn't
    # really wanted in case of a unit test.
    option embedded-server && edo rm "${IMAGE}"/usr/$(exhost --target)/bin/test-connect-t

    # Change from root:root to mysql:mysql so it can be accessed by the server.
    edo chown mysql:mysql "${IMAGE}"/usr/$(exhost --target)/lib/mysql/plugin/auth_pam_tool_dir
    # Above we set INSTALL_MYSQLTESTDIR to an empty string to not install the mysql-test directory.
    # plugin/auth_pam/testing doesn't respect this so we have to manually remove it for now.
    edo rm -rf "${IMAGE}"/suite

#    edo mv "${IMAGE}"/usr/bin/mysqlaccess.conf "${IMAGE}"/etc/mysql/mysqlaccess.conf
#    edo chmod 644 "${IMAGE}"/etc/mysql/mysqlaccess.conf

    # Move pkgconfig file into correct dir, there's no variable we could set
    edo mv "${IMAGE}"/usr/$(exhost --target)/lib/{mysql,}/pkgconfig


    keepdir /var/{lib,log}/mysql
    edo chown mysql:mysql "${IMAGE}"/var/{lib,log}/mysql
    edo chmod 755 "${IMAGE}"/var/{lib,log}/mysql

    hereenvd 46${PN} <<EOF
LDPATH=/usr/@TARGET@/lib/mysql
EOF

    insinto /etc/logrotate.d
    newins "${FILES}"/logrotate.${PN} ${PN}

    install_s6-rc_files
}

