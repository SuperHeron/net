# Copyright 2008, 2009, 2011, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=rakshasa tag=v${PV} ]

SUMMARY="A BitTorrent library for unix."
DESCRIPTION='
LibTorrent is a BitTorrent library written in C++ for *nix, with a focus on high performance and
good code. The library differentiates itself from other implementations by transfering directly
from file pages to the network stack. On high-bandwidth connections it is able to seed at 3 times
the speed of the official client.
'

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="
    debug
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    test:
        dev-cpp/cppunit"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-aligned
    --enable-openssl        # Use OpenSSL's SHA1 implementation
)

