Upstream: yes
Reason: Fix install paths

From 585c8e4a3f0a8614345f51a03df82f2feb2c9ddb Mon Sep 17 00:00:00 2001
From: Alexander Koeppe <format_c@online.de>
Date: Sat, 18 Apr 2020 10:46:40 +0200
Subject: [PATCH] Put /etc under INSTALL_PREFIX on MacOSX, FreeBSD and Windows

---
 CMakeLists.txt | 1 +
 1 file changed, 1 insertion(+)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 0d48623d..5521c6f7 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -113,6 +113,7 @@ set(INSTALL_DATADIR ${INSTALL_PREFIX}/share CACHE PATH "Data installation direct
 set(INSTALL_EXECPREFIX ${INSTALL_PREFIX} CACHE PATH "")
 set(INSTALL_BINDIR  ${INSTALL_PREFIX}/bin CACHE PATH "Binary files installation directory")
 if(OS_DARWIN OR OS_BSD_FREE OR OS_WINDOWS)
+  set(INSTALL_SYSCONFDIR ${INSTALL_PREFIX}/etc CACHE PATH "System configuration directory")
   set(POLKIT_DIR ${INSTALL_PREFIX}/share/polkit-1/actions/ CACHE PATH "Polkit installation directory")
 else()
 #at least on ubuntu, polkit dir couldn't be /usr/local/share, but should be /usr/share
-- 
2.40.1

From fc6d0c4e51e226629d961769c073e5f6d8a269eb Mon Sep 17 00:00:00 2001
From: Pino Toscano <toscano.pino@tiscali.it>
Date: Fri, 28 May 2021 07:45:01 +0200
Subject: [PATCH] Install app icon in XDG hicolor icon theme

Install the icon of the application in the hicolor XDG icon theme;
this way it can be properly loaded by XDG menus in the currently set
XDG icon theme, without looking in the legacy pixmaps location.

ICON_DIR is changed to refer to the top-level of the XDG hicolor icon
theme rather than the precise location for the application SVGs, so it
can be used in the future to install more icons in hicolor.
---
 CMakeLists.txt                | 2 +-
 share/CMakeLists.txt          | 2 +-
 src/interfaces/gtk/ec_gtk.c   | 2 +-
 src/interfaces/gtk3/ec_gtk3.c | 2 +-
 4 files changed, 4 insertions(+), 4 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 20359c83..05bbeb4a 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -121,7 +121,7 @@ endif()
 set(PKEXEC_INSTALL_WRAPPER org.pkexec.ettercap CACHE PATH "Name of the pkexec action file")
 set(DESKTOP_DIR ${INSTALL_PREFIX}/share/applications/ CACHE PATH "Desktop file installation directory")
 set(METAINFO_DIR ${INSTALL_PREFIX}/share/metainfo/ CACHE PATH "Metainfo file installation directory")
-set(ICON_DIR ${INSTALL_PREFIX}/share/pixmaps CACHE PATH "Icon file installation directory")
+set(ICON_DIR ${INSTALL_PREFIX}/share/icons/hicolor CACHE PATH "XDG hicolor icon theme installation directory")
 set(MAN_INSTALLDIR ${INSTALL_PREFIX}/share/man CACHE PATH "Path for manual pages")
 
 if(NOT DISABLE_RPATH)
diff --git a/share/CMakeLists.txt b/share/CMakeLists.txt
index cf7539b4..55214ec9 100644
--- a/share/CMakeLists.txt
+++ b/share/CMakeLists.txt
@@ -51,6 +51,6 @@ install(FILES
 # since ettercap now uses this file as the application icon
 # we still need the icon even if the desktop file is not being installed
 if(ENABLE_GTK)
-  install(FILES ettercap.svg DESTINATION ${ICON_DIR})
+  install(FILES ettercap.svg DESTINATION ${ICON_DIR}/scalable/apps)
 endif()
 
diff --git a/src/interfaces/gtk/ec_gtk.c b/src/interfaces/gtk/ec_gtk.c
index 1b35435a..a1a8f940 100644
--- a/src/interfaces/gtk/ec_gtk.c
+++ b/src/interfaces/gtk/ec_gtk.c
@@ -919,7 +919,7 @@ static void gtkui_setup(void)
    gtk_window_set_default_size(GTK_WINDOW (window), width, height);
 
    /* set window icon */
-   path = ICON_DIR "/" ICON_FILE;
+   path = ICON_DIR "/scalable/apps/" ICON_FILE;
    if (g_file_test(path, G_FILE_TEST_EXISTS)) {
       gtk_window_set_icon(GTK_WINDOW(window), gdk_pixbuf_new_from_file(path, NULL));
    }
diff --git a/src/interfaces/gtk3/ec_gtk3.c b/src/interfaces/gtk3/ec_gtk3.c
index 9aa7be6c..a3ab2ed6 100644
--- a/src/interfaces/gtk3/ec_gtk3.c
+++ b/src/interfaces/gtk3/ec_gtk3.c
@@ -1138,7 +1138,7 @@ static void gtkui_build_widgets(GApplication* app, gpointer data)
    gtk_window_set_default_size(GTK_WINDOW(window), width, height);
 
    /* set window icon */
-   path = ICON_DIR "/" ICON_FILE;
+   path = ICON_DIR "/scalable/apps/" ICON_FILE;
    if (g_file_test(path, G_FILE_TEST_EXISTS)) {
       gtk_window_set_icon(GTK_WINDOW(window), gdk_pixbuf_new_from_file(path, NULL));
    }
-- 
2.40.1

From ce2035c5abb84a8afd273a453045f98efe955989 Mon Sep 17 00:00:00 2001
From: Dan Church <amphetamachine@gmail.com>
Date: Fri, 3 Feb 2023 10:39:44 -0600
Subject: [PATCH] Use GNUInstallDirs for installation paths

No guessing.
---
 CMakeLists.txt | 24 +++++++++++++-----------
 1 file changed, 13 insertions(+), 11 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 7e98b3dc..08e93072 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -3,6 +3,8 @@ project(ettercap C)
 
 set(VERSION "0.8.4-rc")
 
+include(GNUInstallDirs)
+
 set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/Modules")
 set(CMAKE_SCRIPT_PATH "${CMAKE_SOURCE_DIR}/cmake/Scripts")
 
@@ -107,23 +109,23 @@ include(EttercapLibCheck)
 include(EttercapVariableCheck)
 
 set(INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX} CACHE PATH "Installation prefix")
-set(INSTALL_SYSCONFDIR /etc CACHE PATH "System configuration directory")
-set(INSTALL_LIBDIR ${INSTALL_PREFIX}/lib${LIB_SUFFIX} CACHE PATH "Library installation directory")
-set(INSTALL_DATADIR ${INSTALL_PREFIX}/share CACHE PATH "Data installation directory")
-set(INSTALL_EXECPREFIX ${INSTALL_PREFIX} CACHE PATH "")
-set(INSTALL_BINDIR  ${INSTALL_PREFIX}/bin CACHE PATH "Binary files installation directory")
+set(INSTALL_SYSCONFDIR ${CMAKE_INSTALL_FULL_SYSCONFDIR} CACHE PATH "System configuration directory")
+set(INSTALL_LIBDIR ${CMAKE_INSTALL_FULL_LIBDIR} CACHE PATH "Library installation directory")
+set(INSTALL_DATADIR ${CMAKE_INSTALL_FULL_DATADIR} CACHE PATH "Data installation directory")
+set(INSTALL_EXECPREFIX ${CMAKE_INSTALL_PREFIX} CACHE PATH "")
+set(INSTALL_BINDIR  ${CMAKE_INSTALL_FULL_BINDIR} CACHE PATH "Binary files installation directory")
 if(OS_DARWIN OR OS_BSD_FREE OR OS_WINDOWS)
-  set(INSTALL_SYSCONFDIR ${INSTALL_PREFIX}/etc CACHE PATH "System configuration directory")
-  set(POLKIT_DIR ${INSTALL_PREFIX}/share/polkit-1/actions/ CACHE PATH "Polkit installation directory")
+  set(INSTALL_SYSCONFDIR ${CMAKE_INSTALL_PREFIX}/etc CACHE PATH "System configuration directory")
+  set(POLKIT_DIR ${CMAKE_INSTALL_FULL_DATADIR}/polkit-1/actions/ CACHE PATH "Polkit installation directory")
 else()
 #at least on ubuntu, polkit dir couldn't be /usr/local/share, but should be /usr/share
   set(POLKIT_DIR /usr/share/polkit-1/actions/ CACHE PATH "Polkit installation directory")
 endif()
 set(PKEXEC_INSTALL_WRAPPER org.pkexec.ettercap CACHE PATH "Name of the pkexec action file")
-set(DESKTOP_DIR ${INSTALL_PREFIX}/share/applications/ CACHE PATH "Desktop file installation directory")
-set(METAINFO_DIR ${INSTALL_PREFIX}/share/metainfo/ CACHE PATH "Metainfo file installation directory")
-set(ICON_DIR ${INSTALL_PREFIX}/share/icons/hicolor CACHE PATH "XDG hicolor icon theme installation directory")
-set(MAN_INSTALLDIR ${INSTALL_PREFIX}/share/man CACHE PATH "Path for manual pages")
+set(DESKTOP_DIR ${CMAKE_INSTALL_FULL_DATADIR}/applications/ CACHE PATH "Desktop file installation directory")
+set(METAINFO_DIR ${CMAKE_INSTALL_FULL_DATADIR}/metainfo/ CACHE PATH "Metainfo file installation directory")
+set(ICON_DIR ${CMAKE_INSTALL_FULL_DATADIR}/icons/hicolor CACHE PATH "XDG hicolor icon theme installation directory")
+set(MAN_INSTALLDIR ${CMAKE_INSTALL_FULL_MANDIR} CACHE PATH "Path for manual pages")
 
 if(NOT DISABLE_RPATH)
   # Ensure that, when we link to stuff outside of our build path, we include the
-- 
2.40.1

