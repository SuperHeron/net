# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Richard Brown
# Copyright 2010 Ali Polatel <alip@exherbo.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require python [ blacklist=2 multibuild=false with_opt=true python_opts="[sqlite]" ]
require py-pep517 [ backend=setuptools multibuild=false entrypoints=[ zenmap ] ]
require lua [ multibuild=false with_opt=true whitelist="5.4" ]
require option-renames [ renames=[ 'utils ncat' 'utils ndiff' 'utils nping' ] ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="A utility for scanning networks"
DESCRIPTION="
nmap (Network Mapper) is a utility for network exploration, administration, and
security auditing. It uses IP packets in novel ways to determine which hosts are
available online (host discovery), which TCP/UDP ports are open (port scanning),
and what applications and services are listening on each port (version detection).
It can also identify remote host OS and device types via TCP/IP fingerprinting.
Nmap offers flexible target and port specifications, decoy/stealth scanning for
firewall and IDS evasion, and highly optimized timing algorithms for fast scanning.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/dist/${PNV}.tar.bz2"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changelog.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.html"

LICENCES="NPSL"
SLOT="0"
MYOPTIONS="
    gtk [[
        description = [ Build Zenmap, the GTK+ GUI ]
        requires = [ python ncat ndiff nping ]
    ]]
    ncat [[
        description = [ A feature-packed networking utility which reads and writes data across networks from the command line ]
    ]]
    ndiff [[
        description = [ A tool to aid in the comparison of Nmap scans ]
        requires = [ python ]
    ]]
    nping [[
        description = [ A tool for network packet generation, response analysis and response time measurement ]
    ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( linguas: de es fr hr hu id it ja pl pt_BR ro ru sk )
"

# NOTE(somasis): all tests depend way too much on having unsandboxed network caps!
RESTRICT="test"

# FIXME: bundled libdnet (recommended upstream)
# FIXME: bundled liblinear
DEPENDENCIES="
    build+run:
        dev-libs/libpcap[>=1.7.3]
        dev-libs/pcre2
        net-libs/libssh2[>=1.8.0]
        sys-libs/zlib[>=1.2.8]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    run:
        gtk? ( gnome-bindings/pygobject:3[python_abis:*(-)?] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --with-libpcap
    --with-libpcre
    --with-libssh2
    --with-libz
    --with-openssl
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "gtk zenmap"
    "lua liblua"
    "ncat"
    "ndiff"
    "nping"
)

DEFAULT_SRC_COMPILE_PARAMS=( AR="${AR}" RANLIB="${RANLIB}" )

DEFAULT_SRC_INSTALL_PARAMS=( STRIP=: )

# required for libdnet-stripped
AT_M4DIR=( config )

nmap_src_prepare() {
    # Make sure we install python libs in exec_prefix
    # Don't byte compile python libs on install
    edo sed \
        -e "/cd \$(NDIFFDIR) && \$(PYTHON) setup.py install/s:$: --no-compile --prefix /usr --install-lib $(python_get_sitedir) --install-scripts /usr/$(exhost --target)/bin:" \
        -i Makefile.in

    # Don't install useless scripts
    edo sed \
        -e "/self.create_uninstaller()/d" \
        -i ndiff/setup.py

    # Fix install path of desktop files
    edo sed \
        -e 's:$(prefix)/share:/usr/share:g' \
        -i Makefile.in

    autotools_src_prepare
}

nmap_src_configure() {
    # Avoid having multiple python abis in PYTHON, when we don't use it anyway
    option python || unset PYTHON

    default
}

nmap_src_compile() {
    default

    if option gtk; then
        edo pushd zenmap
        py-pep517_compile_one_multibuild
        edo popd
    fi
}

nmap_src_install() {
    default

    if option gtk; then
        edo pushd zenmap
        py-pep517_install_one_multibuild
        edo popd
    fi

    if option ndiff; then
        python_bytecompile
    fi
}

