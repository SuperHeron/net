Upstream: yes

From a9cf69a0ed7766f56976e48a0c96300e94433511 Mon Sep 17 00:00:00 2001
From: Joel Johnson <mrjoel@lixil.net>
Date: Tue, 1 Feb 2022 16:27:56 -0700
Subject: [PATCH] Use matching soname when building with CMake as Makefile
 (#9178)

This updates the CMake support to additionally symlink the soversion value
to the generated shared library when so generated. This aligns the
generated soversion with that traditionally used by the Makefile build
workflow and provides cross-compatibility irrespective of build approach
used.

The primary version of the non-symlink library retains the actual
(non-SO) project version for clarity and compatibility with
installations built using prior versions of CMake support. An example of
the net resulting symlink structures is shown below, where the most
important aspect is that the symlink matching the embedded SONAME is
present (libprotobuf.so.30 in the example case).

Makefile:

    libprotobuf.so -> libprotobuf.so.30.0.0
    libprotobuf.so.30 -> libprotobuf.so.30.0.0
    libprotobuf.so.30.0.0

CMake:

    libprotobuf.so -> libprotobuf.so.30
    libprotobuf.so.30 -> libprotobuf.so.3.19.0.0
    libprotobuf.so.3.19.0.0

Fixes: #8635
---
 libprotobuf.cmake |  2 ++
 1 files changed, 2 insertions(+)

diff --git a/libprotobuf.cmake b/libprotobuf.cmake
index 668b8c278..42f25668e 100644
--- a/libprotobuf.cmake
+++ b/libprotobuf.cmake
@@ -125,6 +125,8 @@ if(MSVC AND protobuf_BUILD_SHARED_LIBS)
 endif()
 set_target_properties(libprotobuf PROPERTIES
     VERSION ${protobuf_VERSION}
+    # Use only the first SO version component for compatibility with Makefile emitted SONAME.
+    SOVERSION 30
     OUTPUT_NAME ${LIB_PREFIX}protobuf
     DEBUG_POSTFIX "${protobuf_DEBUG_POSTFIX}")
 add_library(protobuf::libprotobuf ALIAS libprotobuf)
-- 
2.35.1

