Title: Move redis conf to /etc/redis
Author: Maxime Sorin <maxime.sorin@clever-cloud.com>
Content-Type: text/plain
Posted: 2023-09-07
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-db/redis

Redis configuration files are now installed in "/etc/redis" instead of "/etc".

Wich allows you to set the right permissions to the redis folder, and to avoid errors like with the
CONFIG REWRITE command which attempts to create a temporary file in /etc before merging into redis.conf.

